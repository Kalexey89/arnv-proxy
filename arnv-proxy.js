'use strict';

const http = require('http');

const config = {
    // Параметры HTTP-сервера (для проксирования запроса)
    server: {
        host: '127.0.0.1',
        port: 80,
        backlog: 128 // максимальное число соединений в очереди
    },
    // Параметры HTTP-клиента (для запроса к сервису с информацией о последнем поезде)
    client: {
        hostname: '10.15.0.15',
        port: 7008,
        path: '/LastTrain',
        timeout: 60 * 1000, // таймаут (в миллисекундах)
        headers: { 'Accept': 'application/json' }
    }
};

function parseDate(key, value) {
    const reISO = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*))(?:Z|(\+|-)([\d|:]*))?$/;
    const reMsAjax = /^\/Date\((d|-|.*)\)[\/|\\]$/;

    if (typeof value === 'string') {
        let a = reISO.exec(value);
        if (a) return new Date(value).getTime();

        a = reMsAjax.exec(value);
        if (a) {
            var b = a[1].split(/[-+,.]/);
            return new Date(b[0] ? +b[0] : 0 - +b[1]).getTime();
        }
    }

    return value;
}

function getLastTrain(options, callback) {
    const request = http.get(options, (response) => {
        let body = '';

        response.on('data', (chunk) => {
            body += chunk;
        });
        response.on('end', () => {
            const source = JSON.parse(body, parseDate);
            let result = {};

            result.number = source.TrainNumber + '' || '000000'; // Номер состава
            result.direction = source.Direction || 0; // Направление движения
            result.timestamp = source.EndDateTime || 0; // Время прохождения точки контроля
            result.cars = []; // Список вагонов

            // Заполнение списка вагонов
            const wagons = source.Wagons;
            if (wagons !== undefined && wagons instanceof Array) {
                for (let wagon of source.Wagons) {
                    result.cars.push({
                        number: wagon.Number + '' || '000000', // Номер вагона
                        axles: wagon.AxlesCount || 0 // Количество колесных пар
                    });
                }
            }

            callback(null, result);
        });
    });
    request.on('error', (error) => {
        callback(error, null);
    });
}

http.createServer((request, response) => {
    if (request.method === 'GET' && request.url.toLowerCase() === '/lasttrain') {
        getLastTrain(config.client, (error, data) => {
            if (error !== null) {
                response.writeHead(500, { 'Content-Type': 'application/json' });
                response.end(JSON.stringify({
                    errorCode: error.name,
                    errorMessage: error.message
                }));
            } else {
                response.writeHead(200, {
                    'Content-Type': 'application/json',
                    'Connection': 'close'
                });
                response.end(JSON.stringify(data));
            }
        });
    } else {
        response.writeHead(404, { 'Content-Type': 'text/plain' });
        rresponse.end('Not Found');
    }
}).listen(config.server, () => {
    console.info('Server listen on ' + config.server.host + ':' + config.server.port);
});